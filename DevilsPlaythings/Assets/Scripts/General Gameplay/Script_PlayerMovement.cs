﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Script_PlayerMovement : MonoBehaviour
{
    public Script_LevelManager levelManager;
    public GameObject[] path;
    public Transform target;
    public Vector3 startPosition;
    public Vector3 direction;
    public GameObject pauseMenu;
    public int spaceImOn = 0;
    public int numberOfSpacesToGo;
    public float speed;
    public float levelImIn = 0;
    public bool alterMovement;

    public Color myColor;
    public int randomStar = 0;
    public int numberOfBoardSpaces = 0;
    private bool paused = false;

    private void Start()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<Script_LevelManager>();
        if (SceneManager.GetActiveScene().name == "Scene_Test")
        {
            levelImIn = 0;
            numberOfBoardSpaces = 42;
        }
        if (SceneManager.GetActiveScene().name == "Scene_Circle 1 - Limbo")
        {
            levelImIn = 1;
            numberOfBoardSpaces = 60;
        }
        if (SceneManager.GetActiveScene().name == "Scene_Circle 2 - Lust")
        {
            levelImIn = 2;
            numberOfBoardSpaces = 42;
        }
        if (SceneManager.GetActiveScene().name == "Scene_Circle 3 - Gluttony")
        {
            levelImIn = 3;
            numberOfBoardSpaces = 42;
        }
        if (SceneManager.GetActiveScene().name == "Scene_Circle 4 - Greed")
        {
            levelImIn = 4;
            numberOfBoardSpaces = 42;
        }
        if (SceneManager.GetActiveScene().name == "Scene_Circle 5 - Wrath")
        {
            levelImIn = 5;
            numberOfBoardSpaces = 20;
        }
        if (SceneManager.GetActiveScene().name == "Scene_Circle 6 - Heresy")
        {
            levelImIn = 6;
            numberOfBoardSpaces = 42;
        }
        if (SceneManager.GetActiveScene().name == "Scene_Circle 7 - Violence")
        {
            levelImIn = 7;
            numberOfBoardSpaces = 42;
        }
        if (SceneManager.GetActiveScene().name == "Scene_Circle 8 - Fraud")
        {
            levelImIn = 8;
            numberOfBoardSpaces = 42;
        }
        if (SceneManager.GetActiveScene().name == "Scene_Circle 9 - Treachery")
        {
            levelImIn = 9;
            numberOfBoardSpaces = 42;
        }
        StartLevel();
        startPosition = transform.position;      
    }

    public void StartLevel()
    {
        pauseMenu = GameObject.FindGameObjectWithTag("PauseMenu");
        pauseMenu.GetComponent<Image>().enabled = false;
        pauseMenu.transform.GetChild(0).GetComponent<Text>().enabled = false;
        //your new path is an array of gameobject slots
        path = new GameObject[numberOfBoardSpaces];
        //fill the array with the board spaces
        path = GameObject.FindGameObjectsWithTag("Path").OrderBy(go => go.name).ToArray();
        //the target is now the space I want to be on, on the path
        target = path[spaceImOn].transform;
    }

    public void UpdateOnScene()
    {
        paused = false;
        StartLevel();
        path[randomStar].GetComponent<Script_Waypoints>().myCurrentColor = Color.white;
        path[randomStar].GetComponent<Script_Waypoints>().imAStar = true;
        path[randomStar].GetComponent<Script_Waypoints>().myBoardSpace.GetComponent<Renderer>().material.color =
            Color.white;
    }

    /// <summary>
    /// Move the player
    /// </summary>
    public void StartMoving()
    {       
        InvokeRepeating("PlayerMove", 0.25f, 0.003f);
    }

    private void PlayerMove()
    {
        //start moving

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName(levelManager.boardName))
        {
            direction = target.GetComponent<Renderer>().bounds.center - transform.position;

            transform.Translate(direction.normalized * speed * Time.deltaTime);

            //if I reach the end of this path
            if (spaceImOn == path.Length - 1)
            {
                //reset to original position
                spaceImOn = 0;
                transform.position = startPosition;
                //the space target for th player is reset
                target = path[spaceImOn].transform;
            }

            //if I reach the target path space
            if (Vector3.Distance(transform.position, path[spaceImOn].GetComponent<Renderer>().bounds.center) <= 0.05f)
            {
                //increment the index to the next target
                spaceImOn++;
                //decrements how many space to have left to move
                numberOfSpacesToGo--;
                //move the target
                target = path[spaceImOn].transform;

                GetComponent<Script_Player>().mySpawnPoint.transform.position = gameObject.transform.position;

                //if you hit a star path
                if (path[spaceImOn - 1].GetComponent<Script_Waypoints>().imAStar == true)
                {
                    paused = true;
                }

                //if you have no spaces left to move
                if (numberOfSpacesToGo == 0)
                {
                    myColor = path[spaceImOn].GetComponent<Renderer>().material.color;
                    CancelInvoke();
                }
            }
        }
    }

    public void PressA()
    {
        levelManager.FindStar();
        GetComponent<Script_Player>().silverPieces -= 5;
        GetComponent<Script_Player>().souls++;
        pauseMenu.GetComponent<Image>().enabled = false;
        pauseMenu.transform.GetChild(0).GetComponent<Text>().enabled = false;
        paused = false;
        Time.timeScale = 1;
    }

    public void PressB()
    {
        pauseMenu.GetComponent<Image>().enabled = false;
        pauseMenu.transform.GetChild(0).GetComponent<Text>().enabled = false;
        paused = false;
        Time.timeScale = 1;
    }

    private void Update()
    {
        //if you're not in the board scene(you're in a minigame)
        if (SceneManager.GetActiveScene() != SceneManager.GetSceneByName(levelManager.boardName))
        {
            //your path will be filled with you's, as a placeholder
            for (int i = 0; i < path.Length; i++)
            {
                path[i] = gameObject;
            }
        }
        //if you are in the board scene
        else
        {
            //if paused
            if (paused)
            {
                if (Time.timeScale == 1)
                {
                    pauseMenu.GetComponent<Image>().enabled = true;
                    pauseMenu.transform.GetChild(0).GetComponent<Text>().enabled = true;
                    pauseMenu.transform.GetChild(0).GetComponent<Text>().text = "Would you like to buy a star? It costs 5 silver pieces. Press A to buy, B to exit.";
                    Time.timeScale = 0;
                }
                else if (Time.timeScale == 0)
                {
                    if (GetComponent<Script_Player>().originalController == 1)
                    {
                        if ((Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetKeyDown(KeyCode.T)) && GetComponent<Script_Player>().silverPieces >= 5)
                        {
                            PressA();
                        }
                        if (Input.GetKeyDown(KeyCode.Joystick1Button1) || Input.GetKeyDown(KeyCode.Y))
                        {
                            PressB();
                        }
                    }
                    if (GetComponent<Script_Player>().originalController == 2)
                    {
                        if ((Input.GetKeyDown(KeyCode.Joystick2Button0) || Input.GetKeyDown(KeyCode.T)) && GetComponent<Script_Player>().silverPieces >= 5)
                        {
                            PressA();
                        }
                        if (Input.GetKeyDown(KeyCode.Joystick2Button1) || Input.GetKeyDown(KeyCode.Y))
                        {
                            PressB();
                        }
                    }
                    if (GetComponent<Script_Player>().originalController == 3)
                    {
                        if ((Input.GetKeyDown(KeyCode.Joystick3Button0) || Input.GetKeyDown(KeyCode.T)) && GetComponent<Script_Player>().silverPieces >= 5)
                        {
                            PressA();
                        }
                        if (Input.GetKeyDown(KeyCode.Joystick3Button1) || Input.GetKeyDown(KeyCode.Y))
                        {
                            PressB();
                        }
                    }
                    if (GetComponent<Script_Player>().originalController == 4)
                    {
                        if ((Input.GetKeyDown(KeyCode.Joystick4Button0) || Input.GetKeyDown(KeyCode.T)) && GetComponent<Script_Player>().silverPieces >= 5)
                        {
                            PressA();
                        }
                        if (Input.GetKeyDown(KeyCode.Joystick4Button1) || Input.GetKeyDown(KeyCode.Y))
                        {
                            PressB();
                        }
                    }
                }
            }

            //if you're in the Test level
            if (levelImIn == 0)
            {
                if (spaceImOn == 30)
                {
                    spaceImOn = 32;
                    target = path[spaceImOn].transform;
                }
                if (alterMovement)
                {
                    if (spaceImOn == 12)
                    {
                        spaceImOn = 31;
                        target = path[spaceImOn].transform;
                    }
                }
            }
            //else if you're in Limbo
            else if (levelImIn == 1)
            {
                if (alterMovement)
                {
                    if (spaceImOn == 13)
                    {
                        spaceImOn = 34;
                        target = path[spaceImOn].transform;
                    }
                }
            }
            //else if you're in Lust 
            else if (levelImIn == 2)
            {

            }
            //else if you're in Gluttony
            else if (levelImIn == 3)
            {

            }
            //else if you're in Greed
            else if (levelImIn == 4)
            {

            }
            //else if you're in Wrath
            else if (levelImIn == 5)
            {

            }
            //else if you're in Heresy
            else if (levelImIn == 6)
            {

            }
            //else if you're in Violence
            else if (levelImIn == 7)
            {

            }
            //else if you're in Fraud
            else if (levelImIn == 8)
            {

            }
            //else if you're in Treachery
            else if (levelImIn == 9)
            {

            }
        }
    }

    #region Green Space Effects
    public void SwapColors()
    {
        foreach (GameObject pathTile in path)
        {
            if (pathTile.GetComponent<Renderer>().material.color == Color.blue)
                pathTile.GetComponent<Renderer>().material.color = Color.red;
            else if (pathTile.GetComponent<Renderer>().material.color == Color.red)
                pathTile.GetComponent<Renderer>().material.color = Color.blue;
        }
    }

    public void ChangeDirection()
    { 

    }

    public void CoinLoss()
    {

    }

    public void ControlledPickFromHat()
    {

    }

    public void RandomColor()
    {

    }

    public void SpecialGrouping()
    {

    }

    public void RandomStar()
    {
        //make sure you change the older star back
        path[randomStar].GetComponent<Renderer>().material.color = path[randomStar].GetComponent<Script_Waypoints>().myOriginalColor;
        path[randomStar].GetComponent<Script_Waypoints>().imAStar = false;
        path[randomStar].GetComponent<Script_Waypoints>().myBoardSpace.GetComponent<Renderer>().material.color =
            path[randomStar].GetComponent<Script_Waypoints>().myOriginalColor;
        

        //create new star space
        System.Random shuffle = new System.Random();
        randomStar = shuffle.Next(0, numberOfBoardSpaces);
        path[randomStar].GetComponent<Script_Waypoints>().myCurrentColor = Color.white;
        path[randomStar].GetComponent<Script_Waypoints>().imAStar = true;
        path[randomStar].GetComponent<Script_Waypoints>().myBoardSpace.GetComponent<Renderer>().material.color = 
            Color.white;
    }
    #endregion
}
