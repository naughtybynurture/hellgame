﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Script_LevelManager : MonoBehaviour {

    public static Script_LevelManager instance = null;
    public GameObject[] players = new GameObject[4];
    public List<GameObject> playerOrder;
    public List<GameObject> soulWinners = new List<GameObject>();
    public List<GameObject> silverWinners = new List<GameObject>();
    public Script_MagicHat deck;
    public Script_Turns currentTurn;
    System.Random randomTeamsOrLevels = new System.Random();
    public Text player1Text;
    public Text player2Text;
    public Text player3Text;
    public Text player4Text;
    public GameObject transitionScreen;
    public bool readyForMiniGames;
    public bool needScene;
    public int turns;
    public int levelImIn = 0;
    public int playersStillIn = 4;
    public int minigamePick = 0;
    public int numberOfMiniGames = 2;
    public int numberOfTurnsLeft = 50;
    public int redSpaces = 0;
    public int blueSpaces = 0;
    public string playerWhoWOn;
    public string boardName;
    public string minigameName;
    public string[] FFAMinigames = new string[2];
    public string[] OneVThreeMinigames = new string[2];
    public string[] TwoVTwoMinigames = new string[2];

    private bool gameStarted;
    private bool gameEnded;
    private float delayTimer = 0;

    /// <summary>
    /// Makes the LevelManager a Singleton
    /// </summary>
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Initializes the beginning scene components
    /// </summary>
    void Start()
    {
        
        numberOfTurnsLeft = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Script_GameManager>().numberOfTurns;
        transitionScreen = GameObject.FindGameObjectWithTag("Transition");
        transitionScreen.GetComponent<SpriteRenderer>().enabled = false;
        playerOrder = new List<GameObject>();
        deck = new Script_MagicHat();
        currentTurn = GetComponent<Script_Turns>();
        needScene = true;
        boardName = SceneManager.GetActiveScene().name;
        currentTurn = GetComponent<Script_Turns>();
        DeterminingTurnOrder();
        FindText();
        FindStar();
        FFAMinigames[0] = "Scene_MiniGameJumpRope";
        FFAMinigames[1] = "Scene_MiniGameSpiritBomb";
        FFAMinigames[2] = "Scene_MiniGameHotPotato";
    }

    /// <summary>
    /// Determines the order of the players
    /// </summary>
    void DeterminingTurnOrder()
    {
        //mixes the card of the deck instance
        deck.Mix();

        //takes the top card of the deck
        players[0].GetComponent<Script_Player>().turnOrder = deck.PickFromHat();
        players[1].GetComponent<Script_Player>().turnOrder = deck.PickFromHat();
        players[2].GetComponent<Script_Player>().turnOrder = deck.PickFromHat();
        players[3].GetComponent<Script_Player>().turnOrder = deck.PickFromHat();

        //add in the all the players to a transformable list(playerOrder)
        foreach (GameObject player in players)
        {
            playerOrder.Add(player);
        }

        //orders the player, from highest card to lowest
        playerOrder = playerOrder.OrderByDescending(x => x.GetComponent<Script_Player>().turnOrder).ToList();
    }
    private void Update()
    {
        //if we're in the board game area
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName(boardName))
        {
            if (needScene)
            {
                FindText();
                currentTurn.playerRoll = GameObject.Find("PlayersRoll").GetComponent<Text>();
                needScene = false;
                playerOrder[0].GetComponent<Script_PlayerMovement>().UpdateOnScene();
                playerOrder[1].GetComponent<Script_PlayerMovement>().UpdateOnScene();
                playerOrder[2].GetComponent<Script_PlayerMovement>().UpdateOnScene();
                playerOrder[3].GetComponent<Script_PlayerMovement>().UpdateOnScene();
                redSpaces = 0;
                blueSpaces = 0;
            }
            if (player1Text != null)
            {
                FindText();
                player1Text.text =
                    "- Pigglicorn - \n" +
                    " Silver: " + players[0].GetComponent<Script_Player>().silverPieces +
                    " || Souls: " + players[0].GetComponent<Script_Player>().souls;
                player2Text.text =
                    "- Angel - \n" +
                    " Silver: " + players[1].GetComponent<Script_Player>().silverPieces +
                    " || Souls: " + players[1].GetComponent<Script_Player>().souls;
                player3Text.text =
                    "- Tree - \n" +
                    " Silver: " + players[2].GetComponent<Script_Player>().silverPieces +
                    " || Souls: " + players[2].GetComponent<Script_Player>().souls;
                player4Text.text =
                    "- Woah Rio - \n" +
                    " Silver: " + players[3].GetComponent<Script_Player>().silverPieces +
                    " || Souls: " + players[3].GetComponent<Script_Player>().souls;
            }
            if (numberOfTurnsLeft == 0 && !gameEnded)
            {
                gameEnded = true;
                GameEnded();
            }
        }
        //else if we're in a minigame
        else
        {
            transitionScreen = GameObject.FindGameObjectWithTag("Transition");
            transitionScreen.GetComponent<SpriteRenderer>().enabled = false;
            if (playersStillIn == 1 || playersStillIn == 0)
            {
                transitionScreen.GetComponent<SpriteRenderer>().enabled = true;
                //transitionScreen.transform.GetChild(0).GetComponent<Text>().enabled = true;

                //Starts time after a 3 second delay
                if (delayTimer <= 3.0f)
                {
                    if((Input.GetKey(KeyCode.Joystick1Button0) || Input.GetKey(KeyCode.Z)) &&
                       (Input.GetKey(KeyCode.Joystick2Button0) || Input.GetKey(KeyCode.X)) &&
                       (Input.GetKey(KeyCode.Joystick3Button0) || Input.GetKey(KeyCode.C)) &&
                       (Input.GetKey(KeyCode.Joystick4Button0) || Input.GetKey(KeyCode.V)))
                    {
                        //finds the minigame script and takes it off the players
                        Script_MinigameCoordinator minigameCoord = GameObject.Find("Camera").GetComponent<Script_MinigameCoordinator>();
                        minigameCoord.Destroy();

                        ///When the minigame ends, turn on the players renderers,
                        ///move them to their spawn points, and destroy any minigame scripts
                        FindPlayerSpawn(0);
                        FindPlayerSpawn(1);
                        FindPlayerSpawn(2);
                        FindPlayerSpawn(3);
                        
                        SceneManager.LoadScene(boardName);
                        needScene = true;
                        transitionScreen.GetComponent<SpriteRenderer>().enabled = false;
                        readyForMiniGames = false;
                        playersStillIn = 4;
                        if (numberOfTurnsLeft > 0)
                        {
                            currentTurn.StartTurns();

                        }
                        delayTimer = 0;
                    }
                }
                delayTimer *= Time.deltaTime;
            }
        }

        if (Input.GetKeyDown(KeyCode.A) && !gameStarted)
        {
            Debug.Log("Game has started");
            gameStarted = true;
            currentTurn.StartTurns();
        }

        //if all four players are ready for minigames(they've all taken turns)
        if(playerOrder[0].GetComponent<Script_Player>().readyForMinigames == true &&
           playerOrder[1].GetComponent<Script_Player>().readyForMinigames == true &&
           playerOrder[2].GetComponent<Script_Player>().readyForMinigames == true &&
           playerOrder[3].GetComponent<Script_Player>().readyForMinigames == true)
        {
            //level manager is ready for minigames
            readyForMiniGames = true;
            //all the players are set back to not ready
            playerOrder[0].GetComponent<Script_Player>().readyForMinigames = false;
            playerOrder[1].GetComponent<Script_Player>().readyForMinigames = false;
            playerOrder[2].GetComponent<Script_Player>().readyForMinigames = false;
            playerOrder[3].GetComponent<Script_Player>().readyForMinigames = false;
        }

        //if the level manager is now ready for minigames
        if (readyForMiniGames)
        {
            //turn on transition screen
            transitionScreen.GetComponent<SpriteRenderer>().enabled = true;

            //if all the players hold A and are in the gameboard still
            if ((Input.GetKey(KeyCode.Joystick1Button0) || Input.GetKey(KeyCode.Z)) &&
                (Input.GetKey(KeyCode.Joystick2Button0) || Input.GetKey(KeyCode.X)) &&
                (Input.GetKey(KeyCode.Joystick3Button0) || Input.GetKey(KeyCode.C)) &&
                (Input.GetKey(KeyCode.Joystick4Button0) || Input.GetKey(KeyCode.V)) &&
                SceneManager.GetActiveScene() == SceneManager.GetSceneByName(boardName))
            {
                readyForMiniGames = false;
                //turn on transition screen
                transitionScreen.GetComponent<SpriteRenderer>().enabled = false;
                //creates a new random number class
                System.Random randomlevel = new System.Random();
                //finds a random number between your two values(exclusive)
                minigamePick = randomlevel.Next(0, 3);
                minigameName = FFAMinigames[0];

                //finds the "color" of the player for teams
                foreach(GameObject player in playerOrder)
                {
                    //if I am a certain color, add the number of players to that team
                    if (player.GetComponent<Script_PlayerMovement>().myColor == Color.red)
                    {
                        redSpaces++;
                    }
                    else if (player.GetComponent<Script_PlayerMovement>().myColor == Color.blue)
                    {
                        blueSpaces++;
                    }
                    else if (player.GetComponent<Script_PlayerMovement>().myColor != Color.red &&
                             player.GetComponent<Script_PlayerMovement>().myColor != Color.blue)
                    {                                     
                        int iHateDash = randomTeamsOrLevels.Next(1, 2);
                        if (iHateDash == 1)
                        {
                            redSpaces++;
                        }
                        else
                        {
                            blueSpaces++;
                        }
                    }
                }

                //if its a FFA minigame
                if (redSpaces == 4 || redSpaces == 0)
                {
                    //minigamePick = randomTeamsOrLevels.Next(1, 4); 
                    SceneManager.LoadScene(minigameName);
                }
                //else if it's a 1v3 minigame
                else if (redSpaces == 3 || redSpaces == 1)
                {
                    //minigamePick = randomTeamsOrLevels.Next(1, 4);
                    //SceneManager.LoadScene(OneVThreeMinigames[minigamePick]);
                    SceneManager.LoadScene(minigameName);
                }
                //else if it's a 2v2 minigame
                else if(redSpaces == 2)
                {
                    //minigamePick = randomTeamsOrLevels.Next(1, 4);
                    //SceneManager.LoadScene(TwoVTwoMinigames[minigamePick]);
                    SceneManager.LoadScene(minigameName);
                }
                
                ///all the players targets are themselves for duration 
                ///of the minigame, because the target needs to be set 
                ///to something
                playerOrder[0].GetComponent<Script_PlayerMovement>().target = playerOrder[0].transform;
                playerOrder[1].GetComponent<Script_PlayerMovement>().target = playerOrder[1].transform;
                playerOrder[2].GetComponent<Script_PlayerMovement>().target = playerOrder[2].transform;
                playerOrder[3].GetComponent<Script_PlayerMovement>().target = playerOrder[3].transform;
                needScene = false;
            }
        }
    }

    public void GameEnded()
    {
        Debug.Log("GAME HAS ENDED");
        int largestScore = 0;
        bool soulsDone = false;
        bool silversReady = false;
        bool finalSay = false;

        if (soulsDone == false)
        {
            for (int i = 0; i < playerOrder.Count; i++)
            {
                if (playerOrder[i].GetComponent<Script_Player>().souls > largestScore)
                {
                    largestScore = playerOrder[i].GetComponent<Script_Player>().souls;
                    soulWinners.Clear();
                    soulWinners.Add(playerOrder[i]);
                }
                else if (playerOrder[i].GetComponent<Script_Player>().souls == largestScore)
                {
                    soulWinners.Add(playerOrder[i]);
                }
            }
            silversReady = true;
            soulsDone = true;         
        }

        if (silversReady)
        {
            largestScore = 0;
            for (int i = 0; i < soulWinners.Count; i++)
            {
                if (soulWinners[i].GetComponent<Script_Player>().silverPieces > largestScore)
                {
                    largestScore = soulWinners[i].GetComponent<Script_Player>().silverPieces;
                    silverWinners.Clear();
                    silverWinners.Add(soulWinners[i]);
                }
                else if (soulWinners[i].GetComponent<Script_Player>().silverPieces > largestScore)
                {
                    silverWinners.Add(soulWinners[i]);
                }
            }
            finalSay = true;
            silversReady = false;
        }

        if (finalSay)
        {
            if (soulWinners.Count > 1)
            {
                if (silverWinners.Count == 1)
                {
                    Debug.Log("The winner is " 
                                + silverWinners[0].name);
                }
                else if (silverWinners.Count == 2)
                {
                    Debug.Log("The winners are " 
                                + silverWinners[0].name +
                        " and " + silverWinners[1].name);
                }
                else if (silverWinners.Count == 3)
                {
                    Debug.Log("The winners are " 
                                + silverWinners[0].name + 
                        " and " + silverWinners[1].name +
                        " and " + silverWinners[2].name);
                }
                else if(silverWinners.Count == 4)
                {
                    Debug.Log("The winners are " 
                                + silverWinners[0].name +
                        " and " + silverWinners[1].name +
                        " and " + silverWinners[2].name +
                        " and " + silverWinners[3].name);
                }
            }
            else if (soulWinners.Count == 1)
            {
                Debug.Log("The winner is " + soulWinners[0].name);
            }
        }
    }

    /// <summary>
    /// Finds the text box for each player
    /// </summary>
    void FindText()
    {
        player1Text = GameObject.Find("Player 1 Coin").GetComponent<Text>();
        player2Text = GameObject.Find("Player 2 Coin").GetComponent<Text>();
        player3Text = GameObject.Find("Player 3 Coin").GetComponent<Text>();
        player4Text = GameObject.Find("Player 4 Coin").GetComponent<Text>();
        transitionScreen = GameObject.FindGameObjectWithTag("Transition");
        transitionScreen.GetComponent<SpriteRenderer>().enabled = false;
    }

    public void FindStar()
    {
        players[0].GetComponent<Script_PlayerMovement>().RandomStar();
        players[1].GetComponent<Script_PlayerMovement>().randomStar =
            players[0].GetComponent<Script_PlayerMovement>().randomStar;
        players[2].GetComponent<Script_PlayerMovement>().randomStar =
            players[0].GetComponent<Script_PlayerMovement>().randomStar;
        players[3].GetComponent<Script_PlayerMovement>().randomStar =
            players[0].GetComponent<Script_PlayerMovement>().randomStar;
    }

    public void FindPlayerSpawn(int playerCount)
    {
        playerOrder[playerCount].transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
        playerOrder[playerCount].transform.position = playerOrder[playerCount].GetComponent<Script_Player>().mySpawnPoint.transform.position;
        Destroy(playerOrder[playerCount].GetComponent<Script_MinigameCoordinator>());
    }
}
