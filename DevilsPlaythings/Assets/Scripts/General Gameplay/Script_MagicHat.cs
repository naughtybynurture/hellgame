﻿using System.Collections.Generic;
using UnityEngine;

public class Script_MagicHat
{
    public List<int> hat  = new List<int>();

    /// <summary>
    /// shuffles the deck
    /// </summary>
    public void Mix()
    {
        hat.Add(1);
        hat.Add(2);
        hat.Add(3);
        hat.Add(4);
        hat.Add(5);
        hat.Add(6);
        hat.Add(7);
        hat.Add(8);
        hat.Add(9);
        hat.Add(10);

        //new random instance
        System.Random shuffle = new System.Random();
        //our length will be the hats length
        int length = hat.Count;

        for (int i = length - 1; i > 0; i--)
        {
            //picks a number from 1 to 10
            int random = shuffle.Next(1, i + 1);
            //a pick is made that is whatever number in the pile you're on now
            int pick = hat[i];
            //the number you're on now is changed to a random number
            hat[i] = hat[random];
            ///that random number in the pile is put 
            ///whereever the original place in the pile was
            hat[random] = pick;
        }
    }


    /// <summary>
    /// player picks a card from the hat
    /// </summary>
    /// <returns>a numbered card</returns>
    public int PickFromHat()
    {
        //player picks from the hat
        int playersOrder = hat[1];
        //that card is removed from the hat
        hat.Remove(0);
        //return the card picked
        return playersOrder;
    }
}
