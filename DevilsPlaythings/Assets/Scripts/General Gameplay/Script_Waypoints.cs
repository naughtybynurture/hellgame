﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Waypoints : MonoBehaviour
{
    public Color myCurrentColor;
    public Color myOriginalColor;
    public Script_PlayerManager playerManager;
    public Script_PlayerMovement playerMovement;
    public GameObject levelManager;
    public GameObject door1;
    public GameObject door2;
    public GameObject door3;
    public int myGroup;
    public bool imAStar;
    public GameObject myBoardSpace;

    private void Awake()
    {
        myCurrentColor = GetComponent<Renderer>().material.color;
        myOriginalColor = GetComponent<Renderer>().material.color;
        levelManager = GameObject.FindGameObjectWithTag("LevelManager");
        playerManager = levelManager.GetComponent<Script_PlayerManager>();
        playerMovement = levelManager.GetComponent<Script_PlayerMovement>();

        if (levelManager.GetComponent<Script_LevelManager>().levelImIn == 0)
        {
            if (playerManager.alterPlayersPath == false)
            {
                door1.GetComponent<MeshRenderer>().enabled = false;
                door2.GetComponent<MeshRenderer>().enabled = true;
            }
            else if (playerManager.alterPlayersPath)
            {
                door1.GetComponent<MeshRenderer>().enabled = true;
                door2.GetComponent<MeshRenderer>().enabled = false;
            }
        }
        if (levelManager.GetComponent<Script_LevelManager>().levelImIn == 1)
        {
        }
        if (levelManager.GetComponent<Script_LevelManager>().levelImIn == 2)
        {
        }
        if (levelManager.GetComponent<Script_LevelManager>().levelImIn == 3)
        {
        }
        if (levelManager.GetComponent<Script_LevelManager>().levelImIn == 4)
        {
        }
        if (levelManager.GetComponent<Script_LevelManager>().levelImIn == 5)
        {
        }
        if (levelManager.GetComponent<Script_LevelManager>().levelImIn == 6)
        {
        }
        if (levelManager.GetComponent<Script_LevelManager>().levelImIn == 7)
        {
        }
        if (levelManager.GetComponent<Script_LevelManager>().levelImIn == 8)
        {
        }
        if (levelManager.GetComponent<Script_LevelManager>().levelImIn == 9)
        {
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        myCurrentColor = GetComponent<Renderer>().material.color;

        //if a space is touched by any of the players
        if (other.gameObject.CompareTag("Player1") ||
           other.gameObject.CompareTag("Player2") ||
           other.gameObject.CompareTag("Player3") ||
           other.gameObject.CompareTag("Player4"))
        {
            //if im on the final space
            if (other.gameObject.GetComponent<Script_PlayerMovement>().numberOfSpacesToGo == 1)
            {
                //if Im not a star
                if (!imAStar)
                {
                    //if my group is 1
                    if (myGroup == 1)
                    {
                        //if I'm in test scene(0), I will alter path a certain way
                        if (levelManager.GetComponent<Script_LevelManager>().levelImIn == 0)
                        {
                            if (playerManager.alterPlayersPath == false)
                            {
                                door1.GetComponent<MeshRenderer>().enabled = false;
                                door2.GetComponent<MeshRenderer>().enabled = true;
                                playerManager.alterPlayersPath = true;
                            }
                            else if (playerManager.alterPlayersPath == true)
                            {
                                door1.GetComponent<MeshRenderer>().enabled = true;
                                door2.GetComponent<MeshRenderer>().enabled = false;
                                playerManager.alterPlayersPath = false;
                            }
                        }
                    }

                    //if my color is red
                    if (myCurrentColor == Color.red)
                    {
                        //take three silver from the player
                        other.gameObject.GetComponent<Script_Player>().silverPieces -= 3;
                    }
                    //if my color is blue
                    if (myCurrentColor == Color.blue)
                    {
                        //give three silver to the player
                        other.gameObject.GetComponent<Script_Player>().silverPieces += 3;
                    }
                    if (myCurrentColor == Color.black)
                    {

                    }
                    if (myCurrentColor == Color.grey)
                    {

                    }
                }
                //But if I am a star!
                else if (imAStar)
                {
                    myCurrentColor = Color.white;
                }
            }
        }
    }
}
