﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_PlayerSingleton : MonoBehaviour
{
    public static Script_PlayerSingleton instance = null;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
}
