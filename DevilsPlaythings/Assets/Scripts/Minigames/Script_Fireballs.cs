﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Fireballs : MonoBehaviour
{
    public float difficultyTimer = 2.0f;
    public float speed = 0;
    public float width = 0;
    public float height = 0;
    public float delay = 3f;
    public float yOffsets = 0.25f;
    public float fireball1ZOffset = 0;
    public float fireball2ZOffset = 0.5f;
    public float fireball3ZOffset = 1f;
    public float fireball4ZOffset = 1.5f;

    private float timeCounter = 0;
    private float delayTimer = 0f;
    private float nextInterval = 0;
    private float z = 0;
    private float x = 0;
    private float y = 0;

	// Use this for initialization
	void Update ()
    {
        //after the delay...
		if(delayTimer > delay)
        {
            //the game timer is updated by the speed 
            timeCounter += Time.deltaTime * speed;
            //the fireballs go in a circle
            x = Mathf.Cos(timeCounter) * width;
            y = Mathf.Sin(timeCounter) * height;

            //everytime the next interval is reached
            if (Time.time > nextInterval)
            {
                //speed is increased by the difficulty thats set in inspector
                nextInterval = Time.time + difficultyTimer;
                speed += difficultyTimer;
            }

            transform.position = new Vector3(x, y, z);

            switch (gameObject.name)
            {
                case "Fireball1":
                    transform.position = new Vector3(x, y + yOffsets, z + fireball1ZOffset);
                    break;
                case "Fireball2":
                    transform.position = new Vector3(x, y + yOffsets, z + fireball2ZOffset);
                    break;
                case "Fireball3":
                    transform.position = new Vector3(x, y + yOffsets, z + fireball3ZOffset);
                    break;
                case "Fireball4":
                    transform.position = new Vector3(x, y + yOffsets, z + fireball4ZOffset);
                    break;
                default:
                    transform.position = new Vector3(x, y, z);
                    break;
            }
        }
        delayTimer += 1.0f * Time.deltaTime;
	}
}
