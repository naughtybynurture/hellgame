﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_JumpRope : MonoBehaviour
{
    //public variables
    public Script_LevelManager levelManager;
    public float maxJumpHeight = 0.5f;
    public float groundHeight;
    public Vector3 groundPosition;
    public float jumpSpeed = 5.0f;
    public float fallSpeed = 2.0f;
    public bool inputJump = false;
    public bool grounded = true;
    public MeshRenderer[] myChildren;
    //private variables
    private bool compared;

    // Use this for initialization
    void Start ()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<Script_LevelManager>();
        groundPosition = transform.position;
        groundHeight = transform.position.y;
        maxJumpHeight = transform.position.y + maxJumpHeight;
        GetComponent<Script_Player>().imStillIn = true;
        myChildren = GetComponentsInChildren<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //if player 1 and grounded...
        if ((Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetKeyDown(KeyCode.U))
            && GetComponent<Script_Player>().originalController == 1 &&
            grounded)
        {
            //I can jump
            inputJump = true;
            //jump
            StartCoroutine("Jump");
        }

        //if player 2 and grounded...
        if ((Input.GetKeyDown(KeyCode.Joystick2Button0) || Input.GetKeyDown(KeyCode.I))
            && GetComponent<Script_Player>().originalController == 2 &&
            grounded)
        {
            //I can jump
            inputJump = true;
            //jump
            StartCoroutine("Jump");
        }

        //if player 3 and grounded...
        if ((Input.GetKeyDown(KeyCode.Joystick3Button0) || Input.GetKeyDown(KeyCode.O))
            && GetComponent<Script_Player>().originalController == 3 &&
            grounded)
        {
            //I can jump
            inputJump = true;
            //jump
            StartCoroutine("Jump");

        }

        //if player 4 and grounded...
        if ((Input.GetKeyDown(KeyCode.Joystick4Button0) || Input.GetKeyDown(KeyCode.P))
           && GetComponent<Script_Player>().originalController == 4 &&
            grounded)
        {
            //I can jump
            inputJump = true;
            //Jump
            StartCoroutine("Jump");

        }
    }


    /// <summary>
    /// Allows the player to jump when on the ground
    /// </summary>
    /// <returns>Waits until end of frame</returns>
    private IEnumerator Jump()
    {
        while (true)
        {
            //if you're higher than the alloted height...
            if (transform.position.y >= maxJumpHeight)
            {
                //you cannot jump
                inputJump = false;
            }

            //if you can jump...
            if (inputJump)
            {
                //jump up at a certain speed
                transform.Translate(Vector3.up * jumpSpeed * Time.smoothDeltaTime);
            }
            //if you cant jump...
            else if (!inputJump)
            {
                //"gravity" is going to bring you back down at a certain speed
                transform.Translate(Vector3.down * fallSpeed * Time.smoothDeltaTime);
                if (transform.position.y <= groundPosition.y)
                {
                    transform.position = groundPosition;
                    StopAllCoroutines();
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }

    /// <summary>
    /// Finds if you're touching an object
    /// </summary>
    /// <param name="other">the object you touch</param>
    private void OnTriggerEnter(Collider other)
    {
        ///if the object you touch is tagged "Fireball" and you 
        ///havent touched it before...
        if (other.CompareTag("Fireball") && compared == false)
        {
            //you've now touched the fireball at least once
            compared = true;
            //you're no longer in the running
            GetComponent<Script_Player>().imStillIn = false;
            //if there's at least two people in the running...
            if (levelManager.playersStillIn >= 2)
            {
                //take one player out
                levelManager.playersStillIn--;
            }
            //you're now invisible.
            gameObject.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
            foreach (var itsRenderer in myChildren)
            {
                itsRenderer.enabled = false;
            }
        }
    }
}
