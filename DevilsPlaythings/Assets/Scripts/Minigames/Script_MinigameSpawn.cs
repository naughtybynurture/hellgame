﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_MinigameSpawn : MonoBehaviour
{
    public Script_LevelManager levelManager;

    // Use this for initialization
    void Awake ()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<Script_LevelManager>();

        if (gameObject.name == "Waypoint1")
        {
            levelManager.playerOrder[0].transform.position = gameObject.transform.position;
        }
        if (gameObject.name == "Waypoint2")
        {
            levelManager.playerOrder[1].transform.position = gameObject.transform.position;
        }
        if (gameObject.name == "Waypoint3")
        {
            levelManager.playerOrder[2].transform.position = gameObject.transform.position;
        }
        if (gameObject.name == "Waypoint4")
        {
            levelManager.playerOrder[3].transform.position = gameObject.transform.position;
        }
    }
	
}
