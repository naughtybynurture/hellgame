﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_MinigameSpiritBomb : MonoBehaviour
{
    
    public Script_LevelManager levelManager;
    public GameObject winningPlayer;
    public GameObject myBomb;
    public int winningNumber;
    public int buttonPresses = 0;
    public float countdown = 15.0f;
    public bool winnerChosen = false;

    private void Start()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<Script_LevelManager>();
        winningPlayer = levelManager.playerOrder[0];
        if (GetComponent<Script_Player>().originalController == 1)
        {
            myBomb = GameObject.Find("Bomb1");
        }
        if (GetComponent<Script_Player>().originalController == 2)
        {
            myBomb = GameObject.Find("Bomb2");
        }
        if (GetComponent<Script_Player>().originalController == 3)
        {
            myBomb = GameObject.Find("Bomb3");
        }
        if (GetComponent<Script_Player>().originalController == 4)
        {
            myBomb = GameObject.Find("Bomb4");
        }
    }

    private void Update()
    {
        if (countdown > 0)
        {
            //user inputs
            if (Input.GetKeyDown(KeyCode.U) &&
                GetComponent<Script_Player>().originalController == 1)
            {
                myBomb.transform.localScale += new Vector3(0.01f, 0.01f, 0.01f);
                buttonPresses++;
            }
            if (Input.GetKeyDown(KeyCode.I) &&
                GetComponent<Script_Player>().originalController == 2)
            {
                myBomb.transform.localScale += new Vector3(0.01f, 0.01f, 0.01f);
                buttonPresses++;
            }
            if (Input.GetKeyDown(KeyCode.O) &&
                GetComponent<Script_Player>().originalController == 3)
            {
                myBomb.transform.localScale += new Vector3(0.01f, 0.01f, 0.01f);
                buttonPresses++;
            }
            if (Input.GetKeyDown(KeyCode.P) &&
                GetComponent<Script_Player>().originalController == 4)
            {
                myBomb.transform.localScale += new Vector3(0.01f, 0.01f, 0.01f);
                buttonPresses++;
            }
            countdown -= 1.0f * Time.deltaTime;
        }
        else
        {
            if (!winnerChosen)
            {
                winnerChosen = true;
                for (int i = 0; i < levelManager.playerOrder.Count; i++)
                {
                    if (levelManager.playerOrder[i].GetComponent<Script_MinigameSpiritBomb>().buttonPresses > winningNumber)
                    {
                        winningNumber = levelManager.playerOrder[i].GetComponent<Script_MinigameSpiritBomb>().buttonPresses;
                        winningPlayer = levelManager.playerOrder[i];
                    }
                }
                GetComponent<Script_Player>().imStillIn = false;
                winningPlayer.GetComponent<Script_Player>().imStillIn = true;
                levelManager.playersStillIn = 1;
            }
        }
    }
}
