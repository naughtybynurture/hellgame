﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Script_MinigameCoordinator : MonoBehaviour
{
    Script_LevelManager levelManager;
    Scene minigame;
    int level;
    public bool destroy;

    // Use this for initialization
    void Start()
    {
        //find the level manager
        levelManager = GameObject.Find("LevelManager").GetComponent<Script_LevelManager>();
        minigame = SceneManager.GetSceneByName(levelManager.minigameName);

        //if mini game is jump rope
        if (minigame.name == "Scene_MiniGameJumpRope")
        {
            level = 1;
            levelManager.playerOrder[0].AddComponent<Script_JumpRope>();
            levelManager.playerOrder[1].AddComponent<Script_JumpRope>();
            levelManager.playerOrder[2].AddComponent<Script_JumpRope>();
            levelManager.playerOrder[3].AddComponent<Script_JumpRope>();
        }
        //else if mini game is race
        else if (minigame.name == "Scene_MiniGameSpiritBomb")
        {
            level = 2;
            levelManager.playerOrder[0].AddComponent<Script_MinigameSpiritBomb>();
            levelManager.playerOrder[1].AddComponent<Script_MinigameSpiritBomb>();
            levelManager.playerOrder[2].AddComponent<Script_MinigameSpiritBomb>();
            levelManager.playerOrder[3].AddComponent<Script_MinigameSpiritBomb>();
        }
        //else if mini game is
        else if (minigame.name == "Scene_MiniGameHotPotato")
        {
            level = 3;
            levelManager.playerOrder[0].AddComponent<Script_MinigameHotPotato>();
            levelManager.playerOrder[1].AddComponent<Script_MinigameHotPotato>();
            levelManager.playerOrder[2].AddComponent<Script_MinigameHotPotato>();
            levelManager.playerOrder[3].AddComponent<Script_MinigameHotPotato>();
        }
        //else if mini game is

        //else if mini game is

        //else if mini game is
    }

    public void Destroy()
    {
        if (level == 1)
        {
            Destroy(levelManager.playerOrder[0].GetComponent<Script_JumpRope>());
            Destroy(levelManager.playerOrder[1].GetComponent<Script_JumpRope>());
            Destroy(levelManager.playerOrder[2].GetComponent<Script_JumpRope>());
            Destroy(levelManager.playerOrder[3].GetComponent<Script_JumpRope>());
        }
        else if (level == 2)
        {
            Destroy(levelManager.playerOrder[0].GetComponent<Script_MinigameSpiritBomb>());
            Destroy(levelManager.playerOrder[1].GetComponent<Script_MinigameSpiritBomb>());
            Destroy(levelManager.playerOrder[2].GetComponent<Script_MinigameSpiritBomb>());
            Destroy(levelManager.playerOrder[3].GetComponent<Script_MinigameSpiritBomb>());
        }
        ///fill in the rest of these levels later.
        ///Script_MinigameWHATVER* should be all you have to do
        else if (level == 3)
        {
            Destroy(levelManager.playerOrder[0].GetComponent<Script_MinigameHotPotato>());
            Destroy(levelManager.playerOrder[1].GetComponent<Script_MinigameHotPotato>());
            Destroy(levelManager.playerOrder[2].GetComponent<Script_MinigameHotPotato>());
            Destroy(levelManager.playerOrder[3].GetComponent<Script_MinigameHotPotato>());
        }
        /*
        else if (level == 4)
        {
            Destroy(levelManager.playerOrder[0].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[1].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[2].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[3].GetComponent<Script_Minigame>());
        }
        else if (level == 5)
        {
            Destroy(levelManager.playerOrder[0].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[1].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[2].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[3].GetComponent<Script_Minigame>());
        }
        else if (level == 6)
        {
            Destroy(levelManager.playerOrder[0].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[1].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[2].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[3].GetComponent<Script_Minigame>());
        }
        else if (level == 7)
        {
            Destroy(levelManager.playerOrder[0].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[1].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[2].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[3].GetComponent<Script_Minigame>());
        }
        else if (level == 8)
        {
            Destroy(levelManager.playerOrder[0].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[1].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[2].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[3].GetComponent<Script_Minigame>());
        }
        else if (level == 9)
        {
            Destroy(levelManager.playerOrder[0].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[1].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[2].GetComponent<Script_Minigame>());
            Destroy(levelManager.playerOrder[3].GetComponent<Script_Minigame>());
        }*/
    }
}
