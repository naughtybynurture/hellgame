﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_EmergencyEnd : MonoBehaviour
{
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.Joystick1Button7) &&
           Input.GetKey(KeyCode.Joystick2Button7) &&
           Input.GetKey(KeyCode.Joystick3Button7) &&
           Input.GetKey(KeyCode.Joystick4Button7))
        {
            Application.Quit();
        }
    }
}
