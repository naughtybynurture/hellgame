﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Cursor : MonoBehaviour
{
    public float walkSpeed = 1f;
    public float rotationSpeed = 1f;
    private float translation;
    private float rotation;
	
	// Update is called once per frame
	void Update ()
    {
        translation = Input.GetAxis("Vertical") * walkSpeed;
        rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, -translation);
        transform.Rotate(0, rotation, 0);
	}
}
