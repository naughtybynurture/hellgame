﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Script_Menus : MonoBehaviour
{

    public string sceneName;
    public int turns;

    public void _StartButt()
    {
        SceneManager.LoadScene("Scene_TurnMenu");
    }
    public void _NextButt()
    {
        SceneManager.LoadScene("Scene_BoardMenu");
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<Script_GameManager>().numberOfTurns = turns;
    }
    public void _CredButt()
    {
        SceneManager.LoadScene("Scene_Credits");
    }

    public void _QuitButt()
    {
        Application.Quit();
    }

    public void _LoadScene()
    {
        SceneManager.LoadScene(sceneName);
    }
}

