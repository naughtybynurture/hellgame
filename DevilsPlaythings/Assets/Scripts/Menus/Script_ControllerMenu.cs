﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Script_ControllerMenu : MonoBehaviour
{
    public string levelName;
    public float walkSpeed = 1f;
    public float rotationSpeed = 1f;

    private float translation;
    private float rotation;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Door"))
        {
            levelName = other.name;
            if (Input.GetKeyUp(KeyCode.JoystickButton0))
            {
                SceneManager.LoadScene(levelName);
            }
        }
    }

    private void Update()
    {
        translation = Input.GetAxis("Vertical") * walkSpeed;
        rotation = Input.GetAxis("Horizontal");
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, -translation);
        transform.Rotate(0, rotation, 0);
    }
}
